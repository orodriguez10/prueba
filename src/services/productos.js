import Axios from "axios";

const RUTA = '/notificaciones'

const END_POINTS = {
    listarProductos: () => Axios.get(`${RUTA}/listar`),


    eliminarProductos(id_carpeta, model){
        return Axios.delete(`${RUTA}/${id_carpeta}/crear`,model)
    },

    crearProductos(id_carpeta, model){
        return Axios.post(`${RUTA}/${id_carpeta}/crear`,model)
    },

    editarProductos(id_carpeta, model){
        return Axios.put(`${RUTA}/${id_carpeta}/crear`,model)
    },

}

export default END_POINTS